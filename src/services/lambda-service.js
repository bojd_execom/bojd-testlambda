import axios from 'axios';


export default {
    callLambda(url, data) {
        let requestObject = {
            data
        };

        return new Promise((resolve, reject) => {
            axios.post(url, requestObject)
                .then(({ data }) => {
                    resolve(data);
                })
                .catch(({ response }) => {
                    reject(response.data);
                });
        });
    }
}